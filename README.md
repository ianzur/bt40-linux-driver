# Epson BT40 linux driver
> vendor=0x04b8 product=0x0d12

### TODO:
- test sensor read on Rpi (through HDMI to USB-C converter)
- write simple c program to read and print accel, gyro, magn values
- use [Fusion](..https://github.com/xioTechnologies/Fusion/tree/main) to estimate head position
- convert to driver

### serialCom.py
> Note: ensure 99*.rules are added then unplug and replug the device

verification of all known serial communication with device (ensure rules are added)

### testSensorRead.py
> Note: see hid-sensor-hub

test read from the available sensor iio 

### 99*.rules
copy `99-EpsonBT40.rules` to `/etc/udev/rules.d/` then the device will appear as a ttyBT40 (a symlink to ttyUSB[0-9]) when plugged in.

### hid-sensor-hub
This module(driver) needs to be loaded to talk to the sensors but it is not included in the rpi image. So build the kernel yourself [build the Rpi kernel](https://www.raspberrypi.com/documentation/computers/linux_kernel.html#build-the-kernel) after enabling HID_SENSOR_HUB along with the associated HID_SENSOR_[ACCEL_3D, GYRO_3D, ...].

ian.zurutuza@gmail.com
26 Sept. 2024
