# BT-40 commands

- getbright
- setbright 
  - [0 - 20] | 50 (auto)

- get2d3d
- set2d3d
  - 0|1 -- (0=2D, 1=3D)

- getmute
- setmute
  - 0|1 -- (0=OFF, 1=ON)

- getautosleep
- enableautosleep
  - 0|1 -- (0=OFF, 1=ON)

- getusersleep
- enableusersleep
  - 0|1 -- (0=OFF, 1=ON)

- getdisplaydistance
- setdisplaydistance
  - ??

- sensor
  - accelerometer
  - gyrometer
  - compass
  - ambient light
  - gravity vector
  - aggregated orientation
