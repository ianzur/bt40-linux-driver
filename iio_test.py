from datetime import datetime
import struct
import time

import iio

def find_sensors(ctx):
    
    sensors = {}

    sensors["accel"] = ctx.find_device("accel_3d")
    sensors["gyro"] = ctx.find_device("gyro_3d") 
    sensors["magn"] = ctx.find_device("magn_3d")
    sensors["gravity"] = ctx.find_device("gravity")
#    sensors["als"] = ctx.find_device("als")

    return sensors 


if __name__ == "__main__":
    
    ctx = iio.Context()
    print(ctx.description)

    sensors = find_sensors(ctx)
    
    buf_size = 1 # number of samples
    sensor_bufs = {}

    for sensor_key in sensors:
        print(f"{sensor_key}: {sensors[sensor_key].name}")
        
        for ch in sensors[sensor_key].channels:
            ch.enabled = True

            ch.attrs['sampling_frequency'] = 100.0
            # iio_info should have a function to decode this
            print(ch.data_format)


        sensor_bufs[sensors[sensor_key].name] = iio.Buffer(sensors[sensor_key], buf_size)
#        x = sensor_bufs[sensors[sensor_key].name].set_blocking_mode(False)
 #       print(x)
        
    
    while True:
        
        for sensor in sensors:
            print(sensor)
            sensor_bufs[sensors[sensor].name].refill()

            # idk why this is the size that it is
#            fullbuf = sensor_bufs[sensors[sensor].name].read() 
#            print(fullbuf, len(fullbuf))

            print(sensors[sensor].buffer_attrs['data_available'].value)
            if int(sensors[sensor].buffer_attrs["data_available"].value) > 0:
 #               print(struct.unpack('<3i1q', fullbuf)) # size is incorrect, unable to upack here

                for ch in sensors[sensor].channels:
                    x = ch.read(sensor_bufs[sensors[sensor].name])
#                    print(ch.id, len(x), type(x))
                    
                    if ch.type == iio.ChannelType.IIO_TIMESTAMP:
                        print(f"\t{ch.id}: {datetime.fromtimestamp(struct.unpack('<q', x)[0] / 1e9)}")
                    else:
                        print(f"\t{ch.id}: {struct.unpack('<i', x)[0]}")
        
        time.sleep(0.1)
