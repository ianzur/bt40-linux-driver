from argparse import ArgumentParser
import serial
import os

NEWLINE = os.linesep
PORT_PATH = '/dev/ttyBT40'
TIMEOUT = 0.5 # seconds

#FIXME: logging pls

def writeAndRead(cmd: str) -> bool:
    with serial.Serial(PORT_PATH, baudrate=9600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, timeout=TIMEOUT, write_timeout=TIMEOUT) as serial_port:
        serial_port.write((cmd + NEWLINE).encode('utf-8'))
        # return header (the command sent) terminates with single return
        response = serial_port.read_until('\r\n'.encode('utf-8'))
        print(f"Command \"{response}\" written to the serial port.")
        # return terminates with double newlines
        response = serial_port.read_until('\r\n\r\n'.encode('utf-8')).strip()
        return response

def getBrightness():
    command = "getbright"
    return writeAndRead(command)

def setBrightness(lvl: int):
    # FIXME: check bounds 0 - 20; 50 = auto
    command = "setbright " + str(lvl) 
    return writeAndRead(command)

def getDisplayMode():
    command = "get2d3d"
    return writeAndRead(command)

def setDisplayMode(mode: str):

    if mode == "2D":
        mode = 0
    elif mode == "3D":
        mode = 1
    else:
        print(f"ERR: mode string({mode}) not understood. Exiting.")
        return

    command = "set2d3d " + str(mode)
    return writeAndRead(command)

def getMute():
    command = "getmute"
    return writeAndRead(command)

def setMute(mode: int):
    command = "setmute " + str(mode)
    return writeAndRead(command)

def getAutoSleep():
    command = "getautosleep"
    return writeAndRead(command)

def setAutoSleep(mode: int):
    command = "enableautosleep " + mode 
    return writeAndRead(command)

def getManualSleep():
    command = "getusersleep"
    return writeAndRead(command)

def setManualSleep(mode: int):
    command = "setusersleep " + str(mode)
    return writeAndRead(command)

def getPixelShift():
    command = "getdisplaydistance"
    return writeAndRead(command)

def setPixelShift(val: int) -> bool:
    #FIXME: https://tech.moverio.epson.com/en/basic_function_sdk/developers_guide/appendix.html#ac4-range
    command = "setdisplaydistance " + str(val)
    return writeAndRead(command)

if __name__ == "__main__":
    parser = ArgumentParser(description="verify all serial communication with the BT40 glasses.\nAll arguments are optional, if you do not provide an argument then the getter function is called")
    parser.add_argument("-m", "--mode", type=str, choices=["2D", "3D"], required=False, help="Display mode")
    parser.add_argument("-dd", "--display_distance", type=int, choices=range(-32,256+1,8), required=False, help="Display distance (pixel shift)")
    parser.add_argument("-a", "--audio_mute", type=str, choices=[0,1], required=False, help="Display mode")
    parser.add_argument("-as", "--auto_sleep", type=int, choices=[0,1], required=False, help="auto sleep function")
    parser.add_argument("-ms", "--manual_sleep", type=int, choices=[0,1], required=False, help="manual sleep function")
    parser.add_argument("-b", "--brightness", type=int, choices=list(range(0,20+1))+[50], required=False, help="brightness level (50=auto)")

    args, _ = parser.parse_known_args()

    if args.mode is not None:
        print(setDisplayMode(args.mode))
    else:
        print(getDisplayMode())


    if args.display_distance is not None:
        print(setPixelShift(args.display_distance))
    else:
        print(getPixelShift())

    if args.audio_mute is not None:
        print(setMute(args.audio_mute))
    else:
        print(getMute())


    if args.auto_sleep is not None:
        print(setAutoSleep(args.auto_sleep))
    else:
        print(getAutoSleep())


    if args.manual_sleep is not None:
        print(setManualSleep(args.manual_sleep))
    else:
        print(getManualSleep())


    if args.brightness is not None:
        print(setBrightness(args.brightness))
    else:
        print(getBrightness())
