from pathlib import Path
import sys
from time import sleep

import sys
import os
import fcntl
import time

"""
$ iio_info
iio:device2: gyro_3d (buffer capable)
		4 channels found:
			anglvel_x:  (input, index: 0, format: le:S32/32>>0)
			5 channel-specific attributes found:
				attr  0: hysteresis value: 0.000000
				attr  1: offset value: 0
				attr  2: raw value: 3430000
				attr  3: sampling_frequency value: 0.000000
				attr  4: scale value: 0.000000017
			anglvel_y:  (input, index: 1, format: le:S32/32>>0)
			5 channel-specific attributes found:
				attr  0: hysteresis value: 0.000000
				attr  1: offset value: 0
				attr  2: raw value: 6720000
				attr  3: sampling_frequency value: 0.000000
				attr  4: scale value: 0.000000017
			anglvel_z:  (input, index: 2, format: le:S32/32>>0)
			5 channel-specific attributes found:
				attr  0: hysteresis value: 0.000000
				attr  1: offset value: 0
				attr  2: raw value: -1190000
				attr  3: sampling_frequency value: 0.000000
				attr  4: scale value: 0.000000017
			timestamp:  (input, index: 3, format: le:S64/64>>0)
"""

accel = Path("/sys/bus/iio/devices/iio:device0/")
gyro = Path("/sys/bus/iio/devices/iio:device1/")
magn = Path("/sys/bus/iio/devices/iio:device2/")

def test():

    accel_x = open(accel / "in_accel_x_raw", 'r')
    accel_y = open(accel / "in_accel_y_raw", 'r')
    accel_z = open(accel / "in_accel_z_raw", 'r')
    
    gyro_x = open(gyro / "in_anglvel_x_raw", 'r')
    gyro_y = open(gyro / "in_anglvel_y_raw", 'r')
    gyro_z = open(gyro / "in_anglvel_z_raw", 'r')
    
    magn_x = open(magn / "in_magn_x_raw", 'r')
    magn_y = open(magn / "in_magn_y_raw", 'r')
    magn_z = open(magn / "in_magn_z_raw", 'r')

    i = 0 

    accel_vals = [0,0,0]
    gyro_vals = [0,0,0]
    magn_vals = [0,0,0]

#    while True:
 #       try:
    accel_vals[0] = accel_x.read() 
    accel_vals[1] = accel_y.read()
    accel_vals[2] = accel_z.read()

    gyro_vals[0] = gyro_x.read()
    gyro_vals[1] = gyro_y.read()
    gyro_vals[2] = gyro_z.read()
    
    magn_vals[0] = magn_x.read()
    magn_vals[1] = magn_y.read()
    magn_vals[2] = magn_z.read()
    
    accel_vals = [int(x) * 98e-9 for x in accel_vals]
    gyro_vals = [int(x) * 17e-9 for x in gyro_vals]
    magn_vals = [int(x) * 1e-7 for x in magn_vals]

    print(f"\t{'x':^5}\t{'y':^5}\t{'z':^5}")
    print(f"accel:\t{accel_vals[0]:<.3f}\t{accel_vals[1]:<.3f}\t{accel_vals[2]:<.3f}")
    print(f"gyro:\t{gyro_vals[0]:<.3f}\t{gyro_vals[1]:<.3f}\t{gyro_vals[2]:<.3f}")
    print(f"magn:\t{magn_vals[0]:<.3f}\t{magn_vals[1]:<.3f}\t{magn_vals[2]:<.3f}")

    accel_x.close()
    accel_y.close()
    accel_z.close()
    
    gyro_x.close()
    gyro_y.close()
    gyro_z.close()
    
    magn_x.close()
    magn_y.close()
    magn_z.close()


if __name__ == "__main__":
    while True:
        test()
        sleep(0.25)

